#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include "libft.h"
#include "get_next_line.h"

int		*open_files(int ac, char **av)
{
	int		*fds;
	int		fd;

	fds = malloc(sizeof(int) * (ac - 1));
	if (fds == NULL)
		return NULL;
	for (int i = 1; i < ac; ++i)
	{
		fd = open(av[i], O_RDONLY);
		if (fd < 0)
			return NULL;
		fds[i - 1] = fd;
	}
	return fds;
}

void	gnl_test(int *fds, int fds_size)
{
	char	*str;
	int		fds_ret[fds_size];
	int		i = 0;
	int		ret;

	str = NULL;
	ret = 0;
	if (fds != NULL) {
		while ()
		// while ((ret = get_next_line(fds[0], &str)) > 0) {
			printf("%s\n",str);
			free(str);
		}
	}
	else {
		// char *lol = ft_strdup("Je mange des pommes");
		// printf("subs: %s\n", ft_strsub(lol, 3, 5));
		while ((ret = get_next_line(0, &str)) > 0) {
			printf("%s\n", str);
			// ft_putnbr(ret);
			free(str);
		}
	}
	printf("Ret == %d\n", ret);
}

int		main(int ac, char **av)
{
	int		*fds;

	if (ac > 1) {
		fds = open_files(ac, av);
		if (fds == NULL) {
			printf("There was an error during files opening...\n");
			return (FAILURE);
		}
		gnl_test(fds, ac-1);
	// CLOSING FDS.
		for (int i = 1; i < ac; ++i)
		{
			printf("Closing fd %d\n", fds[i - 1]);
			close(fds[i - 1]);
		}
		free(fds);
	}
	else {
		gnl_test(NULL);
	}
	return (SUCCESS);
}